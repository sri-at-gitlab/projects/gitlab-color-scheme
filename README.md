# GitLab Color Scheme

GitLab inspired color scheme for popular dev tools

![GitLab Colors](https://i.imgur.com/l6TsLEV.png)

## GitLab Colors

- Light orange
	- #fca121
	- 252, 161, 33
- Orange
	- #fc6d36
	- 252, 109, 38
- Red
	- #db3b21
	- 219, 58, 33
- Purple light
	- #6349cb
	- 110, 73, 203
- Purple
	- #380d75
	- 56, 36, 117
- Gray dark
	- #2e2e2e
	- 46, 46, 46

## Custom Colors

- Gray light
	- #9e9e9e
- White faded
	- #f9f9f9

## Sublime Text 3 - GitLab Light

![Sublime Text - GitLab Light Color Scheme](https://i.imgur.com/MnaJpsw.png)

## Sublime Text 3 - GitLab Dark

![Sublime Text - GitLab Dark Color Scheme](https://i.imgur.com/pZe2lWS.png)

## References

- https://design.gitlab.com/